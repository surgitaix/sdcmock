package org.ornet;

import java.io.IOException;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.StringMetricValue;
import org.ornet.common.StateConfiguration;
import org.ornet.mocking.SDCMockingFactory;
import org.ornet.sdclib.consumer.FutureInvocationState;
import org.ornet.sdclib.consumer.SDCConsumer;
import org.ornet.simulation.NumericStateOperation;
import org.ornet.simulation.SDCSimulationController;
import org.ornet.simulation.SimulationConfiguration;
import org.ornet.simulation.SDCSimulationPlatform;

/**
 *
 * @author besting
 */
public class TestSDCMock {
    
    // Change this to valid IPs (make equal to test local)
    private static final String LOCAL_IP = "192.168.178.151";    
    private static final String REMOTE_IP = "192.168.178.151";    
    
    public void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(TestSDCMock.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testSimulator() throws IOException {
        // Local cluster, only 1 node
        SDCSimulationPlatform.startup(LOCAL_IP, new String [] {LOCAL_IP, REMOTE_IP});
        
        StateConfiguration config = new StateConfiguration();
        config.create("handle0", 0).create("handle1", 1);
        
        final SimulationConfiguration simulationConfiguration = new SimulationConfiguration(config);
        simulationConfiguration.getStateOperations().put("handle0", new NumericStateOperation(NumericStateOperation.Manipulationtype.RANDOM, 10, 0, 1000));
        
        // Create
        SDCSimulationController.createSimulatedRemoteProvider(REMOTE_IP, "EPR_123", simulationConfiguration);
        
        sleep(10000);
        
        // Update
        SDCSimulationPlatform.getSimulations().put("EPR_123", simulationConfiguration);
        
        sleep(10000);
        
        // Remove
        SDCSimulationController.removeSimulatedRemoteProvider(REMOTE_IP, "EPR_123");
        
        sleep(10000);
        
        SDCSimulationPlatform.shutdown();
    }
    
    static SDCConsumer consumer = null; 
    
    @BeforeClass
    public static void setUpClass() {
        consumer = SDCMockingFactory.createMockedConsumer();  
    }    
    
    @Test
    public void testMockMdib() {
        System.out.println("Test MDIB");
        Mdib m = consumer.getMDIB();
        assertNotNull(m);
    }

    @Test
    public void testMockCommitString() {
        System.out.println("Commit String");
        StringMetricValue smv = new StringMetricValue();
        smv.setValue("test_value");
        FutureInvocationState fis = new FutureInvocationState();
        consumer.commitString("test_handle", smv, fis);
        assertTrue(fis.waitReceived(InvocationState.FIN, 1000));
    }
    
}
