package org.ornet.common;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryUpdatedListener;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractState;
import org.ornet.sdclib.provider.SDCProvider;
import org.ornet.simulation.SDCSimulationController;
import org.ornet.simulation.SimulationConfiguration;

/**
 *
 * @author besting
 */
public class SimulationConfigurationEventListener implements EntryUpdatedListener<String, SimulationConfiguration>{

    @Override
    public void entryUpdated(EntryEvent<String, SimulationConfiguration> ee) {
        String epr = ee.getKey();
        SimulationConfiguration config = ee.getValue();
        if (SDCSimulationController.LOCAL_PROVIDERS.containsKey(epr)) {
            Logger.getLogger(SimulationConfigurationEventListener.class.getName()).log(Level.INFO, "Simulation configuration updated.");
            SDCProvider provider = SDCSimulationController.LOCAL_PROVIDERS.get(epr);
            Map<String, AbstractState> states = config.getStateConfig().getStatesCopy();
            states.entrySet().forEach((entry) -> {
                Logger.getLogger(SimulationConfigurationEventListener.class.getName()).log(Level.INFO, "Updating local state: {0}", entry.getKey());
                provider.updateState(entry.getValue());
            });
            // Update state operations in case they've been changed, too
            SDCSimulationController.updateStateOperations(epr, config);
        }
    }
    
}
