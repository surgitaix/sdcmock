package org.ornet.common;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.ornet.cdm.AbstractMetricValue;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.MeasurementValidity;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericMetricValue;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.StringMetricValue;

/**
 * Configuration class, containing methods to simplify the management of states
 *
 * @author stegemann, besting
 */
public class StateConfiguration {
    
    private final Map<String, NumericMetricState> numericStates = new ConcurrentHashMap<>();
    private final Map<String, StringMetricState> stringStates = new ConcurrentHashMap<>();

    public Map<String, AbstractState> getStatesCopy() {
        Map<String, AbstractState> states = new ConcurrentHashMap<>();
        states.putAll(numericStates);
        states.putAll(stringStates);
        return states;
    }

    public StateConfiguration() {
    }
    
    /**
     * Create and add a new StringMetricState
     *
     * @param descriptorHandle State handle
     * @param initialValue Initial Value
     * @return The current object to support method chaining
     */
    public StateConfiguration create(String descriptorHandle, String initialValue) {
        StringMetricState sms = new StringMetricState();
        sms.setActivationState(ComponentActivation.ON);
        sms.setDescriptorHandle(descriptorHandle);
        StringMetricValue smv = new StringMetricValue();
        smv.setValue(initialValue);
        sms.setMetricValue(smv);

        stringStates.put(descriptorHandle, sms);

        return this;
    }

    /**
     * Create and add a new NumericMetricState
     *
     * @param descriptorHandle State handle
     * @param initialValue Initial value
     * @return The current object to support method chaining
     */
    public StateConfiguration create(String descriptorHandle, BigDecimal initialValue) {
        NumericMetricState nms = new NumericMetricState();
        nms.setActivationState(ComponentActivation.ON);
        nms.setDescriptorHandle(descriptorHandle);
        NumericMetricValue nv = new NumericMetricValue();
        nv.setValue(initialValue);
        AbstractMetricValue.MetricQuality amq = new AbstractMetricValue.MetricQuality();
        amq.setValidity(MeasurementValidity.VLD);
        nv.setMetricQuality(amq);
        nms.setMetricValue(nv);

        numericStates.put(descriptorHandle, nms);

        return this;
    }

    /**
     * Convenience method which allows the user to pass a double value instead
     * of a BigDecimal object.
     *
     * @param descriptorHandle State handle
     * @param initialValue Initial Value
     * @return The current object to support method chaining
     */
    public StateConfiguration create(String descriptorHandle, double initialValue) {
        return create(descriptorHandle, BigDecimal.valueOf(initialValue));
    }
}
