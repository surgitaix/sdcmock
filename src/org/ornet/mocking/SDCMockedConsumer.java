package org.ornet.mocking;

import org.ornet.common.StateConfiguration;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.GetMdibResponse;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.Mdib;
import org.ornet.sdclib.binding.BICEPSTypeConverter;
import org.ornet.sdclib.consumer.FutureInvocationState;
import org.ornet.sdclib.consumer.ISDCConsumerOperationInvokedHandler;
import org.ornet.sdclib.consumer.ISDCConsumerStateChangedHandler;
import org.ornet.sdclib.consumer.SDCConsumer;
import org.ornet.sdclib.consumer.SDCConsumerHandler;
import org.ornet.sdclib.provider.SDCProvider;

/**
 * Mocked consumer
 *
 * @author stegemann, besting
 *
 */
public class SDCMockedConsumer extends SDCConsumer {

    protected Map<String, AbstractState> states;
    protected Map<String, SDCConsumerHandler> eventHandler;
    protected MdDescription mdd;
    protected boolean sanityCheck = false;
    protected MockedInitialConfiguration initialConfiguration;

    /**
     * Initialize internal objects
     */
    protected void init() {
        states = new ConcurrentHashMap<>();
        eventHandler = new ConcurrentHashMap<>();

        initialConfiguration = new MockedInitialConfiguration();
    }

    /**
     * Issue a stateChanged-event to the registered handler
     *
     * @param <T>
     * @param state New active state
     */
    public <T extends AbstractState> void notify(T state) {
        SDCConsumerHandler h = this.eventHandler.get(state.getDescriptorHandle());
        if (h != null && h instanceof ISDCConsumerStateChangedHandler) {
            ((ISDCConsumerStateChangedHandler) h).onStateChanged(state);
        }
    }

    /**
     * Issue a stateChangeEvent to the registered handler and invoke the future
     * invocation state.
     *
     * @param handle State handle
     * @param is Invocation state
     * @param fis Future Invocation State
     */
    public void notify(String handle, InvocationState is, FutureInvocationState fis) {
        try {
            Method method = fis.getClass().getDeclaredMethod("setConsumer", SDCConsumer.class);
            method.setAccessible(true);
            method.invoke(fis, this);                    
            Class<? extends FutureInvocationState> fisClass = fis.getClass();
            Method setActual = fisClass.getDeclaredMethod("setActual", InvocationState.class);
            setActual.setAccessible(true);
            setActual.invoke(fis, is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SDCConsumerHandler h = eventHandler.get(handle);
        if (h != null && h instanceof ISDCConsumerOperationInvokedHandler) {
            ((ISDCConsumerOperationInvokedHandler) h).onOperationInvoked(null, is);
        }
    }

    /**
     * Activate runtime sanity checks for the mocked consumer
     *
     * @param check Perform checks
     */
    public void performSanityCheck(boolean check) {
        sanityCheck = check;
    }

    /**
     * Check if generated MDIB is schema conform.
     *
     * @return Result of the schema check
     */
    public boolean isSchemaConform() {
        try {
            GetMdibResponse response = new GetMdibResponse();
            response.setSequenceId("0");
            final Mdib mdib = getMDIB();
            mdib.setSequenceId("0");
            response.setMdib(mdib);
            response.setSequenceId(mdib.getSequenceId());
            BICEPSTypeConverter.toString(response);                    
        }
        catch(Exception e) {
            Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return true;
    }

    /**
     * Returns the initial configuration object
     *
     * @return
     */
    public MockedInitialConfiguration getInitialConfiguration() {
        return initialConfiguration;
    }

    public class MockedInitialConfiguration extends StateConfiguration {

        @Override
        public Map<String, AbstractState> getStatesCopy() {
            return SDCMockedConsumer.this.states;
        }

    }
}
