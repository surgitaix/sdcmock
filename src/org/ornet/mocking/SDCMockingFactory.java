package org.ornet.mocking;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.math.BigInteger;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.StringMetricValue;
import java.util.logging.*;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdState;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.NumericMetricValue;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.consumer.FutureInvocationState;
import org.ornet.sdclib.consumer.SDCConsumer;
import org.ornet.sdclib.consumer.SDCConsumerHandler;
import org.ornet.sdclib.consumer.SDCLifecycleHandler;

public class SDCMockingFactory {

    /**
     * Create a mocked consumer.
     * 
     * @return The consumer
     */
    public static SDCConsumer createMockedConsumer() {
        MdDescription mdd = new MdDescription();
        return createMockedConsumer(mdd);
    }

    public static SDCConsumer createMockedConsumer(MdDescription mdd) {
        SDCMockedConsumer consumer = mock(SDCMockedConsumer.class);
        consumer.init();
        consumer.mdd = mdd;

        when(consumer.getMDState()).then(new Answer<MdState>() {
            @Override
            public MdState answer(InvocationOnMock invocation) throws Throwable {
                MdState mdstate = new MdState();
                consumer.states.entrySet().forEach((next) -> {
                    mdstate.getState().add(next.getValue());
                });
                mdstate.getState().addAll(consumer.states.values());
                return mdstate;
            }
        });

        when(consumer.getMDIB()).then(new Answer<Mdib>() {
            @Override
            public Mdib answer(InvocationOnMock invocation) throws Throwable {
                Mdib mdib = new Mdib();
                mdib.setMdDescription(consumer.mdd);
                mdib.setMdState(consumer.getMDState());
                mdib.setSequenceId("0");
                mdib.setMdibVersion(BigInteger.valueOf(0));
                return mdib;
            }
        });

        when(consumer.getMDDescription()).then(new Answer<MdDescription>() {
            @Override
            public MdDescription answer(InvocationOnMock invocation) throws Throwable {
                return consumer.mdd;
            }
        });

        when(consumer.requestState(anyString(), anyObject())).thenAnswer(new Answer<AbstractState>() {
            @Override
            public AbstractState answer(InvocationOnMock invocation) throws Throwable {
                return consumer.states.get(invocation.getArgumentAt(0, String.class));
            }
        });

        when(consumer.commitState(any(AbstractState.class), any(FutureInvocationState.class))).thenAnswer(new Answer<InvocationState>() {
            @Override
            public InvocationState answer(InvocationOnMock invocation) throws Throwable {
                AbstractState arg = invocation.getArgumentAt(0, AbstractState.class);
                FutureInvocationState fis = invocation.getArgumentAt(1, FutureInvocationState.class);
                if (arg == null) {
                    return InvocationState.FAIL;
                } else {
                    // Check set requirements
                    if (consumer.sanityCheck) {
                        AbstractMetricDescriptor descriptor = SDCToolbox.findReferencedMetricDescritor(consumer, arg);
                        if (descriptor == null) {
                            // Log
                            Logger.getGlobal().log(Level.SEVERE, "Referenced metric descriptor not found:{0}.", arg.getDescriptorHandle());
                            consumer.notify(arg.getDescriptorHandle(), InvocationState.FAIL, fis);
                            return InvocationState.FAIL;
                        }

                        if (!SDCToolbox.isMetricChangeAllowed(consumer, (AbstractMetricState) arg)) {
                            // Log
                            Logger.getGlobal().log(Level.SEVERE, "Metric change not allowed:{0}.", arg.getDescriptorHandle());

                            consumer.notify(arg.getDescriptorHandle(), InvocationState.FAIL, fis);
                            return InvocationState.FAIL;
                        }
                    }
                }
                consumer.states.put(arg.getDescriptorHandle(), arg);
                consumer.notify(arg);
                consumer.notify(arg.getDescriptorHandle(), InvocationState.WAIT, fis);
                consumer.notify(arg.getDescriptorHandle(), InvocationState.START, fis);
                consumer.notify(arg.getDescriptorHandle(), InvocationState.FIN, fis);
                return InvocationState.WAIT;
            }
        });

        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                SDCConsumerHandler handler = invocation.getArgumentAt(0, SDCConsumerHandler.class);
                consumer.eventHandler.put(handler.getDescriptorHandle(), handler);
                return null;
            }
        }).when(consumer).addEventHandler(any(SDCConsumerHandler.class));

        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                consumer.eventHandler.remove(invocation.getArgumentAt(0, String.class));
                return null;
            }
        }).when(consumer).removeAllEventHandlers(any(String.class));

        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                SDCConsumerHandler handler = invocation.getArgumentAt(0, SDCConsumerHandler.class);
                consumer.eventHandler.remove(handler.getDescriptorHandle());
                return null;
            }
        }).when(consumer).removeEventHandler(any(SDCConsumerHandler.class));

        Mockito.doNothing().when(consumer).close();

        Mockito.doNothing().when(consumer).addLifecycleHandler(any(SDCLifecycleHandler.class));

        Mockito.doNothing().when(consumer).removeLifecycleHandler(any(SDCLifecycleHandler.class));

        Mockito.doCallRealMethod().when(consumer).commitString(any(String.class), any(StringMetricValue.class), any(FutureInvocationState.class));

        Mockito.doCallRealMethod().when(consumer).commitValue(any(String.class), any(NumericMetricValue.class), any(FutureInvocationState.class));

        Mockito.doCallRealMethod().when(consumer).init();

        Mockito.doCallRealMethod().when(consumer).notify(anyObject());

        Mockito.doCallRealMethod().when(consumer).notify(any(String.class), any(InvocationState.class), any(FutureInvocationState.class));

        Mockito.doCallRealMethod().when(consumer).notify(any(String.class), any(InvocationState.class), any(FutureInvocationState.class));

        Mockito.doCallRealMethod().when(consumer).isSchemaConform();

        Mockito.doCallRealMethod().when(consumer).performSanityCheck(anyBoolean());

        Mockito.doCallRealMethod().when(consumer).getInitialConfiguration();

        consumer.init();

        return consumer;
    }

}
