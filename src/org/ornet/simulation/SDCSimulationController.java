package org.ornet.simulation;

import java.math.BigDecimal;
import org.ornet.simulation.commands.StopSimulatedProviderRemoteRunnable;
import org.ornet.simulation.commands.StartSimulatedProviderRemoteRunnable;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.sdclib.SDCFluent;
import org.ornet.sdclib.SDCToolbox;
import org.ornet.sdclib.provider.SDCFluentStateChangeRequestContext;
import org.ornet.sdclib.provider.SDCProvider;

public class SDCSimulationController {
    
    private static final int INITIAL_DELAY_OPERATIONS = 5000;    
    
    public static final Map<String, SDCProvider> LOCAL_PROVIDERS = new ConcurrentHashMap<>();
    public static final Map<String, ScheduledExecutorService> STATE_OPERATION_SERVICES = new ConcurrentHashMap<>();

    public static boolean createSimulatedRemoteProvider(String ip, String epr, SimulationConfiguration config) {
        return SDCSimulationPlatform.executeRemote(ip, new StartSimulatedProviderRemoteRunnable(epr, config));
    }
   
    public static boolean removeSimulatedRemoteProvider(String ip, String epr) {
        return SDCSimulationPlatform.executeRemote(ip, new StopSimulatedProviderRemoteRunnable(epr));       
    }
    
    public static void removeSimulatedLocalProvider(String epr) {
        SDCFluent.DeleteLocalMember(epr);
        LOCAL_PROVIDERS.remove(epr);
        if (STATE_OPERATION_SERVICES.containsKey(epr)) {
            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Removing current scheduled operations for {0}", epr);
            STATE_OPERATION_SERVICES.get(epr).shutdownNow();
            STATE_OPERATION_SERVICES.remove(epr);
        }        
    }

    public static void createSimulatedLocalProvider(String epr, SimulationConfiguration config) {
        SDCProvider [] providerArray = new SDCProvider[1];
        BlockingDeque<SDCFluentStateChangeRequestContext> deque = SDCFluent.CreateLocalMember(epr, config.createDescription(), config.getStateConfig().getStatesCopy().values(), providerArray);
        SDCFluent.HandleAsyncRequests(deque, (state) -> {
            state.endNotify(InvocationState.FIN);
        });
        LOCAL_PROVIDERS.put(epr, providerArray[0]);
        SDCSimulationPlatform.simulations.put(epr, config);
        updateStateOperations(epr, config);
    }  

    public static void updateStateOperations(String epr, SimulationConfiguration config) {
        // Check for current entry, shutdown executor service if needed
        if (STATE_OPERATION_SERVICES.containsKey(epr)) {
            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Removing current scheduled operations for {0}", epr);
            STATE_OPERATION_SERVICES.get(epr).shutdownNow();
        }
        if (!LOCAL_PROVIDERS.containsKey(epr)) {
            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Provider closed - nothing to do for {0}", epr);
            STATE_OPERATION_SERVICES.remove(epr);
            return;
        }
        // Create new executor service and schedule operations for all handles
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        STATE_OPERATION_SERVICES.put(epr, ses);
        config.getStateOperations().forEach((handle, sop) -> {
            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Scheduling auto operation for {0}, handle {1}", new Object [] {epr, handle});
            ses.scheduleWithFixedDelay(() -> {
                try {
                    if (!LOCAL_PROVIDERS.containsKey(epr)) {
                        // Provider has been shut down, we have to remove all operations scheduled in the service
                        Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Provider closed - removing current scheduled operations for {0}", epr);
                        ses.shutdownNow();
                        STATE_OPERATION_SERVICES.remove(epr);
                        return;
                    }
                    // Change value
                    SDCProvider provider = LOCAL_PROVIDERS.get(epr);
                    if (sop.getType() == NumericStateOperation.Manipulationtype.RANDOM) {
                        AbstractMetricState ams = SDCToolbox.findMetricState(provider, handle);
                        double newValue = ThreadLocalRandom.current().nextDouble(sop.getLowerBound(), sop.getUpperBound());
                        if (ams instanceof NumericMetricState) {
                            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.INFO, "Changing state {0} to random value {1}", new Object [] {handle, newValue});
                            NumericMetricState nms = (NumericMetricState)ams;
                            nms.getMetricValue().setValue(BigDecimal.valueOf(newValue));
                            provider.updateState(nms);
                        } else {
                            Logger.getLogger(SDCSimulationController.class.getName()).log(Level.SEVERE, "State type not supported for handle {0}", handle);
                        }
                    }
                }
                catch (Exception e) {
                    Logger.getLogger(SDCSimulationController.class.getName()).log(Level.SEVERE, null, e);
                }
            }, INITIAL_DELAY_OPERATIONS, sop.getDelayTimeMs(), TimeUnit.MILLISECONDS);            
        });
    }  
    
}
