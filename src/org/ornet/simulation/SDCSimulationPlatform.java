package org.ornet.simulation;

import com.hazelcast.config.Config;
import com.hazelcast.config.InterfacesConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IExecutorService;
import com.hazelcast.core.IMap;
import com.hazelcast.core.Member;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.common.SimulationConfigurationEventListener;
import org.ornet.sdclib.SDCFluent;

/**
 *
 * @author besting
 */
public class SDCSimulationPlatform {
    
    private static final String SDC_MOCK = "SDCMock";
    private static final String SDC_EXEC = "SDCExec";
    private static final String SDC_DATA = "SDCData";

    private static Config config;
    private static HazelcastInstance hazelcastInstance;
    private static IExecutorService executor;
    private static SimulationConfigurationEventListener configListener;
    
    protected static IMap<String, SimulationConfiguration> simulations;
    
    public static void main(String [] args) {
        String ip = getArg(args, "bind=");
        if (ip != null) {
            ip = ip.substring(ip.indexOf('=') + 1);
        }
        String cluster = getArg(args, "cluster=");
        String [] clusterList = null;
        if (cluster != null) {
            cluster = cluster.substring(cluster.indexOf('=') + 1);
            clusterList = cluster.split(",");
        }        
        startup(ip, clusterList);
    }

    public static IMap<String, SimulationConfiguration> getSimulations() {
        return simulations;
    }
    
    private static String getArg(String[] args, String prefix) {
        if (args == null) {
            return null;
        }
        for (String next : args) {
            if (next.startsWith(prefix)) {
                return next;
            }
        }
        return null;
    }    
    
    public static void startup(String bind, String [] cluster) {
        Logger.getLogger(SDCSimulationPlatform.class.getName()).log(Level.INFO, "SDCMock 1.0 (c) SurgiTAIX AG");
        SDCFluent.JoinSDCNetwork(false);
        config = new Config(SDC_MOCK);
        NetworkConfig network = config.getNetworkConfig();
        if (bind != null) {
            InterfacesConfig ifConfig = network.getInterfaces();
            ifConfig.setEnabled(true);
            ifConfig.addInterface(bind);            
        }
        JoinConfig join = network.getJoin();
        final TcpIpConfig tcpIpConfig = join.getTcpIpConfig();
        tcpIpConfig.setEnabled(cluster != null);
        for (String ip : cluster) {
            tcpIpConfig.addMember(ip);
        }
        join.getAwsConfig().setEnabled(false);
        final MulticastConfig multicastConfig = join.getMulticastConfig();    
        multicastConfig.setEnabled(cluster == null);  
        multicastConfig.setMulticastGroup("224.2.2.3");
        multicastConfig.setMulticastPort(16111);
        
        hazelcastInstance = Hazelcast.newHazelcastInstance(config);
        executor = hazelcastInstance.getExecutorService(SDC_EXEC);
        simulations = hazelcastInstance.getMap(SDC_DATA);
        
        configListener = new SimulationConfigurationEventListener();
        simulations.addEntryListener(configListener, true);
    }
    
    public static void shutdown() {
        SDCFluent.LeaveSDCNetwork();
    }
    
    public static boolean executeRemote(String ip, RemoteRunnable runnable) {
        for (Member member : hazelcastInstance.getCluster().getMembers()) {
            try {
                if (member.getAddress().getInetAddress().getHostAddress().contains(ip)) {
                    executor.executeOnMember(runnable, member);
                    return true;
                }
            } catch (UnknownHostException ex) {
                Logger.getLogger(SDCSimulationPlatform.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
}
