package org.ornet.simulation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.CodedValue;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MetricAvailability;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.StringMetricDescriptor;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.common.StateConfiguration;

public class SimulationConfiguration implements Serializable {

    private static final long serialVersionUID = -8107905455719233003L;
    
    private Map<String, NumericStateOperation> stateOperations = new ConcurrentHashMap<>();
    
    private transient StateConfiguration stateConfig;
    private static final transient Gson GSON = new GsonBuilder().enableComplexMapKeySerialization().create();

    private void writeObject(java.io.ObjectOutputStream stream) throws IOException {
        String json = GSON.toJson(stateConfig);
        stream.writeUTF(json);
        json = GSON.toJson(stateOperations);
        stream.writeUTF(json);
    }

    private void readObject(java.io.ObjectInputStream stream) throws IOException, ClassNotFoundException {
        String json = stream.readUTF();
        stateConfig = GSON.fromJson(json, StateConfiguration.class);
        json = stream.readUTF();
        stateOperations = GSON.fromJson(json, new TypeToken<Map<String, NumericStateOperation>>(){}.getType());
    }
    
    public SimulationConfiguration(StateConfiguration initialConfig) {
        this.stateConfig = initialConfig;
    }

    public StateConfiguration getStateConfig() {
        return stateConfig;
    } 

    public Map<String, NumericStateOperation> getStateOperations() {
        return stateOperations;
    }
    
    public MdDescription createDescription() {
        MdDescription mdd = new MdDescription();
        MdsDescriptor mds = new MdsDescriptor();
        mds.setHandle("handle_mds");
        VmdDescriptor vmd = new VmdDescriptor();
        vmd.setHandle("handle_vmd");
        mds.getVmd().add(vmd);
        ChannelDescriptor chn = new ChannelDescriptor();
        chn.setHandle("handle_channel");
        vmd.getChannel().add(chn);
        stateConfig.getStatesCopy().entrySet().forEach((entry) -> {
            String handle = entry.getKey();
            AbstractState state = entry.getValue();
            if (state instanceof NumericMetricState) {
                chn.getMetric().add(createNumericDescriptor(handle));
            }
            else if (state instanceof StringMetricState) {
                chn.getMetric().add(createStringDescriptor(handle));
            }
        });
        mdd.getMds().add(mds);
        return mdd;
    }

    private static AbstractMetricDescriptor createNumericDescriptor(String handle) {
        NumericMetricDescriptor nmd = new NumericMetricDescriptor();
        nmd.setMetricCategory(MetricCategory.SET);
        nmd.setMetricAvailability(MetricAvailability.CONT);
        CodedValue unit = new CodedValue();
        unit.setCodingSystem("N/A");
        unit.setCode("N/A");
        nmd.setUnit(unit);
        CodedValue type = new CodedValue();
        type.setCodingSystem("N/A");
        type.setCode("N/A");
        nmd.setType(type);
        nmd.setHandle(handle);
        nmd.setResolution(BigDecimal.ONE);
        return nmd;
    }
    
    private static AbstractMetricDescriptor createStringDescriptor(String handle) {
        StringMetricDescriptor smd = new StringMetricDescriptor();
        smd.setMetricCategory(MetricCategory.SET);
        smd.setMetricAvailability(MetricAvailability.CONT);
        CodedValue unit = new CodedValue();
        unit.setCodingSystem("N/A");
        unit.setCode("N/A");
        smd.setUnit(unit);
        CodedValue type = new CodedValue();
        type.setCodingSystem("N/A");
        type.setCode("N/A");
        smd.setType(type);
        smd.setHandle(handle);
        return smd;
    }  
    
    
}
