package org.ornet.simulation;

import java.io.Serializable;

public interface RemoteRunnable extends Runnable, Serializable {
      
    static final long serialVersionUID = -483991412884705022L;
    
    @Override
    public void run();
    
}
