package org.ornet.simulation.commands;

import org.ornet.simulation.RemoteRunnable;
import org.ornet.simulation.SDCSimulationController;

/**
 *
 * @author besting
 */
public class StopSimulatedProviderRemoteRunnable implements RemoteRunnable {

    private static final long serialVersionUID = 2164290150870439947L;
    
    private final String epr;

    public StopSimulatedProviderRemoteRunnable(String epr) {
        this.epr = epr;
    }
    
    @Override
    public void run() {
        SDCSimulationController.removeSimulatedLocalProvider(epr);
    }
    
}
