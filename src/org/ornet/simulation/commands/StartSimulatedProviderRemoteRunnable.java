package org.ornet.simulation.commands;

import org.ornet.simulation.RemoteRunnable;
import org.ornet.simulation.SDCSimulationController;
import org.ornet.simulation.SimulationConfiguration;

/**
 *
 * @author besting
 */
public class StartSimulatedProviderRemoteRunnable implements RemoteRunnable {

    private static final long serialVersionUID = -6485738603406238083L;
    
    private final String epr;
    private final SimulationConfiguration config;

    public StartSimulatedProviderRemoteRunnable(String epr, SimulationConfiguration config) {
        this.epr = epr;
        this.config = config;
    }
    
    @Override
    public void run() {
        SDCSimulationController.createSimulatedLocalProvider(epr, config);
    }
    
}
