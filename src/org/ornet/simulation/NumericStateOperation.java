package org.ornet.simulation;

/**
 * Numeric state operation. Defines how and when values automatically change at runtime in the simulated consumer.
 * 
 * @author besting
 */
public class NumericStateOperation {
    
    public enum Manipulationtype {
        RANDOM
    }
    
    private Manipulationtype type;
    private double upperBound;
    private double lowerBound;
    private int delayTimeMs;

    public NumericStateOperation(Manipulationtype type, double upperBound, double lowerBound, int delayTimeMs) {
        this.type = type;
        this.upperBound = upperBound;
        this.lowerBound = lowerBound;
        this.delayTimeMs = delayTimeMs;
    }
    
    public Manipulationtype getType() {
        return type;
    }

    public void setType(Manipulationtype type) {
        this.type = type;
    }

    public double getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(double upperBound) {
        this.upperBound = upperBound;
    }

    public double getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(double lowerBound) {
        this.lowerBound = lowerBound;
    }    

    public int getDelayTimeMs() {
        return delayTimeMs;
    }

    public void setDelayTimeMs(int delayTimeMs) {
        this.delayTimeMs = delayTimeMs;
    }
    
}
